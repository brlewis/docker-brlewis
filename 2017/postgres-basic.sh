#!/bin/sh

# docker pull postgres
docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres
sleep 10
docker exec -u postgres some-postgres psql -c 'select CURRENT_TIMESTAMP, CURRENT_USER'
docker rm -f some-postgres
